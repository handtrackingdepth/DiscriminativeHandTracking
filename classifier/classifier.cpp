#include "classifier.hpp"
#include <Sherwood.h>
#include <opencv2/opencv.hpp>
#include <imageList.hpp>
#include <iomanip>

const cv::Size imageSize(640, 480);
std::random_device rd;
std::default_random_engine generator(rd());

const float backgroundDepth = 0.9798275f; // meterscv 2

void writeDebug(int nodeindex, std::vector<unsigned int> &leftIndices, std::vector<unsigned int> &rightIndices, const sw::IDataPointCollection &data, const sw::IFeatureResponse &feat, float thresh);

// Depth image feature with label
class DepthImageData : public sw::IDataPointCollection
{
public:
    DepthImageData(ImageList &images, bool useAnnotations = true)
            : _images(images), _useAnnotations(useAnnotations)
    {
        // Take random points uniformly distributed from each input image
        std::uniform_int_distribution<int> xpixel(0, imageSize.width - 1);
        std::uniform_int_distribution<int> ypixel(0, imageSize.height - 1);

        if(_useAnnotations) // Using annotations, take all hand pixels and equal num of random background pixels
        {
            for(int i = 0; i < _images.count(); i++)
            {
                cv::Mat img, label;
                std::tie(img, label) = _images[i];

                std::vector<cv::Point2i> handPoints;
                cv::findNonZero(label, handPoints);

                float ret;
                std::vector<cv::Point2i> backPoints(handPoints.size());
                std::generate(backPoints.begin(), backPoints.end(), [&]()
                {
                    cv::Point2i pt;
                    do
                    {
                        pt.x = xpixel(generator);
                        pt.y = ypixel(generator);

                        cv::Scalar value = img.at<float>(pt);
                        ret = value.val[0];

                    }
                    while(std::find(handPoints.begin(), handPoints.end(), pt) != handPoints.end() ||
                          std::find(backPoints.begin(), backPoints.end(), pt) != backPoints.end() ||
                          ret >= backgroundDepth);

                    return pt;
                });

                std::transform(handPoints.begin(), handPoints.end(), std::back_inserter(_pts), [&](cv::Point2i pt)
                {
                    return std::make_pair(pt, i);
                });

                std::transform(backPoints.begin(), backPoints.end(), std::back_inserter(_pts), [&](cv::Point2i pt)
                {
                    return std::make_pair(pt, i);
                });
            }
        }
        else // Not using annotations (testing) generate dense labeling
        {
            for(int i = 0; i < _images.count(); i++)
            {
                cv::Mat img, label;
                std::tie(img, label) = _images[i];

                for(int j = 0; j < imageSize.height; j++)
                {
                    for(int k = 0; k < imageSize.width; k++)
                    {
                        cv::Scalar value = img.at<float>(j,k);
                        float ret = value.val[0];

                        if(ret < backgroundDepth)
                            _pts.push_back(std::make_pair(cv::Point2i(k, j), i));
                    }
                }
            }
        }
    }

    virtual unsigned int Count() const
    {
        return static_cast<unsigned int>(_pts.size());
    }

    cv::Point2i pointForIndex(unsigned int index) const
    {
        return _pts[index].first;
    }

    int imageNumberForIndex(unsigned int index) const
    {
        return _pts[index].second;
    }

    unsigned short label(unsigned int index) const
    {
        cv::Point2i pt = pointForIndex(index);
        int im = imageNumberForIndex(index);

        cv::Mat img, label;
        std::tie(img, label) = _images[im];

        cv::Scalar value = label.at<unsigned char>(pt);
        unsigned int ret = value.val[0];

        return static_cast<unsigned short>(ret > 127 ? 1u : 0u);
    }

    float depth(unsigned int index) const
    {
        cv::Point2i pt = pointForIndex(index);
        int im = imageNumberForIndex(index);

        cv::Mat img, label;
        std::tie(img, label) = _images[im];

        cv::Scalar value = img.at<float>(pt);
        float ret = value.val[0];

        return ret;
    }

    float depthAtOffset(cv::Point2i offset, unsigned int index) const
    {
        int im = imageNumberForIndex(index);

        cv::Mat img, label;
        std::tie(img, label) = _images[im];

        cv::Scalar value = img.at<float>(offset);
        float ret = value.val[0];

        return ret;
    }

private:

    ImageList &_images;
    std::vector<std::pair<cv::Point2i, int>> _pts;
    bool _useAnnotations;
};

class ResponseAdapter
{
public:
    virtual cv::Point2i applyNormalization(cv::Point2f pt, float dx) const = 0;
    virtual float finalResponse(float du, float dv) const = 0;
};

// Feature response for the shotton feature
class ShottonAdapter : public ResponseAdapter
{
public:

    virtual cv::Point2i applyNormalization(cv::Point2f pt, float dx) const
    {
        return pt * (1.f / dx);
    }

    virtual float finalResponse(float du, float dv) const
    {
        return du - dv;
    }
};

class UnnormAdapter : public ResponseAdapter
{
    virtual cv::Point2i applyNormalization(cv::Point2f pt, float dx) const
    {
        return pt;
    }

    virtual float finalResponse(float du, float dv) const
    {
        return du - dv;
    }
};

class AbsAdapter : public ResponseAdapter
{
    virtual cv::Point2i applyNormalization(cv::Point2f pt, float dx) const
    {
        return pt;
    }

    virtual float finalResponse(float du, float dv) const
    {
        return std::fabs(du - dv);
    }
};

class DepthComparisonResponse : public sw::IFeatureResponse
{
public:
    DepthComparisonResponse() = default;
    DepthComparisonResponse(cv::Point2f u, cv::Point2f v, FeatureType type) : _type(type), _u(u), _v(v) {}

    static DepthComparisonResponse CreateRandom(sw::Random &r, int lb, int ub, FeatureType type)
    {
        std::uniform_int_distribution<int> randDepthOffset(lb, ub);

        cv::Point2f u;
        u.x = randDepthOffset(generator);
        u.y = randDepthOffset(generator);

        cv::Point2f v;
        v.x = randDepthOffset(generator);
        v.y = randDepthOffset(generator);

        return DepthComparisonResponse(u, v, type);
    }

    float GetResponse(const sw::IDataPointCollection &data, unsigned int sampleIndex) const
    {
        std::unique_ptr<ResponseAdapter> adapter(nullptr);
        switch (_type)
        {
            case FeatureType::Abs:
                adapter.reset(new AbsAdapter());
                break;

            case FeatureType::Shotton:
                adapter.reset(new ShottonAdapter());
                break;

            case FeatureType::Unnorm:
                adapter.reset(new UnnormAdapter());
                break;

        }

        const DepthImageData &depthData = dynamic_cast<const DepthImageData &>(data);
        float dx = depthData.depth(sampleIndex); // Get depth value for current pixel

        cv::Point2f x = depthData.pointForIndex(sampleIndex);

        // Apply Shotton's formula
        // f(x) = d(x + u/d(x)) - d(x + v/d(x))
        cv::Point2i offsetU = adapter->applyNormalization(x + _u, dx);
        cv::Point2i offsetV = adapter->applyNormalization(x + _v, dx);

        // If a point is out-of-bounds give it a large positive value
        cv::Rect bounds(cv::Point2i(0, 0), imageSize);

        float du = 0;
        if(offsetU.inside(bounds))
        {
            du = depthData.depthAtOffset(offsetU, sampleIndex);

            if(du >= backgroundDepth)
                du = std::numeric_limits<float>::max();
        }
        else
        {
            du = std::numeric_limits<float>::max();
        }

        float dv = 0;
        if(offsetV.inside(bounds))
        {
            dv = depthData.depthAtOffset(offsetV, sampleIndex);

            if(dv >= backgroundDepth)
                dv = std::numeric_limits<float>::max();
        }
        else
        {
            dv = std::numeric_limits<float>::max();
        }

        // Compute the final depth feature
        float response = adapter->finalResponse(du, dv);

        return response;
    }

    cv::Point2f _u, _v;
    FeatureType _type;

};

// Adapted directly from sherwood demo
class HistogramAggregator {
private:
    unsigned short bins_[2];
    int binCount_;

    unsigned int sampleCount_;
public:
    double Entropy() const {
        if (sampleCount_ == 0)
            return 0.0;

        double result = 0.0;
        for (int b = 0; b < BinCount(); b++) {
            double p = (double) bins_[b] / (double) sampleCount_;
            result -= p == 0.0 ? 0.0 : p * log(p) / log(2.0);
        }

        return result;
    }

    HistogramAggregator() {
        binCount_ = 0;
        for (int b = 0; b < binCount_; b++)
            bins_[b] = 0;
        sampleCount_ = 0;
    }

    HistogramAggregator(int nClasses) {
        if (nClasses > 2)
            throw std::runtime_error("HistogramAggregator supports a maximum of two classes.");
        binCount_ = nClasses;
        for (int b = 0; b < binCount_; b++)
            bins_[b] = 0;
        sampleCount_ = 0;
    }

    float GetProbability(int classIndex) const {
        return (float) (bins_[classIndex]) / sampleCount_;
    }

    int BinCount() const { return binCount_; }

    unsigned int SampleCount() const { return sampleCount_; }

    int FindTallestBinIndex() const {
        unsigned int maxCount = bins_[0];
        int tallestBinIndex = 0;

        for (int i = 1; i < BinCount(); i++) {
            if (bins_[i] > maxCount) {
                maxCount = bins_[i];
                tallestBinIndex = i;
            }
        }

        return tallestBinIndex;
    }

    // IStatisticsAggregator implementation
    void Clear() {
        for (int b = 0; b < BinCount(); b++)
            bins_[b] = 0;

        sampleCount_ = 0;
    }

    // Max Ehrlich: work with DepthImageData instead of sherwood demo data
    void Aggregate(const sw::IDataPointCollection &data, unsigned int index) {
        DepthImageData &concreteData = (DepthImageData &) (data);

        bins_[concreteData.label(index)]++;
        sampleCount_ += 1;
    }

    void Aggregate(const HistogramAggregator &aggregator) {
        assert(aggregator.BinCount() == BinCount());

        for (int b = 0; b < BinCount(); b++)
            bins_[b] += aggregator.bins_[b];

        sampleCount_ += aggregator.sampleCount_;
    }

    HistogramAggregator DeepClone() const {
        HistogramAggregator result(BinCount());

        for (int b = 0; b < BinCount(); b++)
            result.bins_[b] = bins_[b];

        result.sampleCount_ = sampleCount_;

        return result;
    }
};

class ShottonContext : public sw::ITrainingContext<DepthComparisonResponse,HistogramAggregator>
{
public:
    ShottonContext(int lb, int ub, FeatureType type) : _lb(lb), _ub(ub) {}

    // Implementation of ITrainingContext
    virtual DepthComparisonResponse GetRandomFeature(sw::Random& random)
    {
        return DepthComparisonResponse::CreateRandom(random, _lb, _ub, _type);
    }

    HistogramAggregator GetStatisticsAggregator()
    {
        return HistogramAggregator(2);
    }

    // Adapted from sherwood demo
    double ComputeInformationGain(const HistogramAggregator& allStatistics, const HistogramAggregator& leftStatistics, const HistogramAggregator& rightStatistics)
    {
        double entropyBefore = allStatistics.Entropy();

        unsigned int nTotalSamples = leftStatistics.SampleCount() + rightStatistics.SampleCount();

        if (nTotalSamples <= 1)
            return 0.0;

        double entropyAfter = (leftStatistics.SampleCount() * leftStatistics.Entropy() + rightStatistics.SampleCount() * rightStatistics.Entropy()) / nTotalSamples;

        return entropyBefore - entropyAfter;
    }

    // Adapted from sherwood demo
    bool ShouldTerminate(const HistogramAggregator& parent, const HistogramAggregator& leftChild, const HistogramAggregator& rightChild, double gain)
    {
        return gain < 0.0001;
    }

private:
    int _lb, _ub;
    FeatureType _type;
};

DecisionForest::DecisionForest(std::string modelFileName)
{
    std::ifstream test(modelFileName);

    if(test)
    {
        test.close();
        model = sw::Forest<DepthComparisonResponse, HistogramAggregator>::Deserialize(modelFileName);
    }
}

void DecisionForest::train(ImageList &images, FeatureType type, std::map<std::string, int> params)
{
    sw::Random r;
    DepthImageData data(images, true);

    ShottonContext context(params["lowerBound"], params["upperBound"], type);

    sw::TrainingParameters parameters;
    parameters.Verbose = true;
    parameters.NumberOfTrees = params["ntrees"];
    parameters.MaxDecisionLevels = params["depth"];
    parameters.NumberOfCandidateFeatures = params["candidateFeatures"];
    parameters.NumberOfCandidateThresholdsPerFeature = params["candidateThresholds"];

    model = sw::ForestTrainer<DepthComparisonResponse, HistogramAggregator>::TrainForest(r, parameters, context, data, 0, writeDebug);
}

std::vector<cv::Mat> DecisionForest::test(ImageList &images)
{
    if(model.get() != nullptr)
    {
        sw::Random r;
        DepthImageData data(images, false);

        int nClasses = model->GetTree(0).GetNode(0).TrainingDataStatistics.BinCount();

        std::vector<std::vector<int>> leafIndicesPerTree;
        model->Apply(data, leafIndicesPerTree, 0);

        std::vector<cv::Mat> labeledImages;

        int currentIdx = -1;
        cv::Mat label;

        for (unsigned int i = 0; i < data.Count(); i++)
        {
            int idx = data.imageNumberForIndex(i);

            if(idx != currentIdx)
            {
                if(label.rows != 0)
                    labeledImages.push_back(label);

                label = cv::Mat(imageSize, CV_32F, cv::Scalar(0.0));
                currentIdx++;
            }

            float sum = 0;
            for (unsigned int t = 0; t < model->TreeCount(); t++)
            {
                int leafIndex = leafIndicesPerTree[t][i];
                sum += model->GetTree(t).GetNode(leafIndex).TrainingDataStatistics.GetProbability(1);
            }

            cv::Point2i pt = data.pointForIndex(i);
            float p = sum / model->TreeCount();
            label.at<float>(pt) = p;
        }

        if(label.rows != 0)
            labeledImages.push_back(label);

        return labeledImages;
    }

    throw std::runtime_error("No model");
}

void DecisionForest::save(std::string modelFileName)
{
    if(model.get() != nullptr)
        model->Serialize(modelFileName);
}

void writeDebug(int nodeindex, std::vector<unsigned int> &leftIndices, std::vector<unsigned int> &rightIndices, const sw::IDataPointCollection &data, const sw::IFeatureResponse &feat, float thresh)
{
    DepthImageData &concreteData = (DepthImageData &) (data);

    DepthComparisonResponse &concreteResponse = (DepthComparisonResponse &)(feat);

    // Mark the left points in blue
    std::map<int, cv::Mat> debugImages;
    std::map<int, cv::Mat> responseImages;
    for(unsigned int i : leftIndices)
    {
        cv::Point pixel = concreteData.pointForIndex(i);
        int index = concreteData.imageNumberForIndex(i);

        if(debugImages[index].empty())
        {
            debugImages[index] = cv::Mat(imageSize, CV_8UC3, cv::Scalar(0));
            responseImages[index] = cv::Mat(imageSize, CV_32F, cv::Scalar(0.0));
        }

        debugImages[index].at<cv::Vec3b>(pixel) = cv::Vec3b(255, 0, 0);

        float response = std::abs(concreteResponse.GetResponse(data, i));
        responseImages[index].at<float>(pixel) = response;
    }

    // mark the right points in red
    for(unsigned int i : rightIndices)
    {
        cv::Point pixel = concreteData.pointForIndex(i);
        int index = concreteData.imageNumberForIndex(i);

        if(debugImages[index].empty())
        {
            debugImages[index] = cv::Mat(imageSize, CV_8UC3, cv::Scalar(0));
            responseImages[index] = cv::Mat(imageSize, CV_32F, cv::Scalar(0.0));
        }

        debugImages[index].at<cv::Vec3b>(pixel) = cv::Vec3b(0, 0, 255);

        float response = std::abs(concreteResponse.GetResponse(data, i));
        responseImages[index].at<float>(pixel) = response;
    }

    // Write the feature information
    cv::Point2f u = concreteResponse._u;
    cv::Point2f v = concreteResponse._v;

    std::ostringstream uss;
    uss << "U = " << u;

    std::ostringstream vss;
    vss << "v = " << v;

    std::string us = uss.str(), vs = vss.str();

    cv::Point upos(imageSize.width - 150, imageSize.height / 2);
    cv::Point vpos(imageSize.width - 150, imageSize.height / 2 + 20);

    // Write the threshold
    cv::Rect blueRect(imageSize.width - 165, imageSize.height / 2 + 28, 15, 15);
    cv::Rect redRect(imageSize.width - 30, imageSize.height / 2 + 28, 15, 15);

    cv::Point tpoint(imageSize.width - 155, imageSize.height / 2 + 40);

    std::ostringstream tss;
    tss.precision(6);
    tss << " < " << std::setprecision(6) << std::setw(7) << std::setfill('0') << thresh << " < ";
    std::string tstring = tss.str();

    // Compute line showing the features
    cv::Point2f start(550, 150);
    cv::Point2i uDemo = start + u;
    cv::Point2i vDemo = start + v;

    // compute the path
    std::vector<std::string> path;
    for(int i = nodeindex; i > 0; i = int(std::floor((double(i) - 1.0) / 2.0)))
    {
        path.push_back(std::to_string(i) + "/");
    }

    // Get the final directory name
    std::string directory = std::accumulate(path.rbegin(), path.rend(), std::string("0/"));

    std::string mkdir = "mkdir -p " + directory;
    int res = system(mkdir.c_str());

    if(!res) {

        for (auto p : debugImages) {
            cv::putText(p.second, us, upos, CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));
            cv::putText(p.second, vs, vpos, CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));

            cv::rectangle(p.second, blueRect, cv::Scalar(255, 0, 0), CV_FILLED);
            cv::rectangle(p.second, redRect, cv::Scalar(0, 0, 255), CV_FILLED);

            cv::putText(p.second, tstring, tpoint, CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));

            cv::line(p.second, start, uDemo, cv::Scalar(0, 255, 0));
            cv::line(p.second, start, vDemo, cv::Scalar(0, 255, 0));

            cv::imwrite(directory + std::to_string(p.first) + "_split.png", p.second);
        }

        for (auto p : responseImages) {
            // Normalize feature responses to be between 0 and 1
            double maxResponse;
            cv::minMaxLoc(p.second, nullptr, &maxResponse);

            cv::Mat normalizedGray;
            p.second.convertTo(normalizedGray, CV_8U, 255.0 / maxResponse);

            // Colormap
            cv::Mat colorMap;
            cv::applyColorMap(normalizedGray, colorMap, cv::COLORMAP_JET);

            // Draw feature info
            cv::putText(colorMap, us, upos, CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));
            cv::putText(colorMap, vs, vpos, CV_FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));

            cv::line(colorMap, start, uDemo, cv::Scalar(0, 255, 0));
            cv::line(colorMap, start, vDemo, cv::Scalar(0, 255, 0));

            // Write
            cv::imwrite(directory + std::to_string(p.first) + "_response.png", colorMap);
        }
    }
}