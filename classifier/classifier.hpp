#pragma once
#include <string>
#include <opencv2/opencv.hpp>
#include <memory>

class HistogramAggregator;
class ShottonResponse;
class ImageList;

namespace MicrosoftResearch { namespace Cambridge { namespace Sherwood {
            template<class F, class A>
            class Forest;
}}}

namespace sw = MicrosoftResearch::Cambridge::Sherwood;

enum FeatureType { Shotton, Unnorm, Abs };

class DepthComparisonResponse;

class DecisionForest
{
public:
    DecisionForest(std::string modelFileName);
    void save(std::string modelFileName);

    void train(ImageList &images, FeatureType type, std::map<std::string, int> params);
    std::vector<cv::Mat> test(ImageList &images);

private:
    std::auto_ptr<sw::Forest<DepthComparisonResponse, HistogramAggregator>> model;

};