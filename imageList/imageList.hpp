#pragma once

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>
#include <fstream>

class ImageList
{
public:
    ImageList(const std::string &file);
    ImageList(cv::Mat singleImage);
    std::pair<cv::Mat, cv::Mat> operator[](unsigned int index) const;

    unsigned int count() const;

private:
    std::vector<std::pair<cv::Mat, cv::Mat>> _images;
};
