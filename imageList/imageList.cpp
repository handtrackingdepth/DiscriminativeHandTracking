#include "imageList.hpp"
#include <algorithm>
#include <iterator>

ImageList::ImageList(const std::string &file)
{
    std::string ext = file.substr(file.find_last_of('.'));

    if(ext == ".lst")
    {
        std::ifstream list(file);
        std::transform(std::istream_iterator<std::string>(list), std::istream_iterator<std::string>(),
                       std::back_inserter(_images), [](std::string imgName)
       {
            cv::Mat image = cv::imread(imgName + ".png", CV_LOAD_IMAGE_ANYDEPTH);
            cv::Mat annotation = cv::imread(imgName + "_mask.png", CV_LOAD_IMAGE_GRAYSCALE);

            if (image.depth() != CV_16U)
                throw std::runtime_error("Only 16U depth maps are supported");

            cv::Mat metersImage;
            image.convertTo(metersImage, CV_32F, 1.f / 1000.f);

            return std::make_pair(metersImage, annotation);
        });

    }
    else if(ext == ".yml")
    {
        cv::FileStorage input(file, cv::FileStorage::READ);

        int size = int(input["size"]);

        _images.resize((unsigned int)(size));

        for(int i = 0; i < size; i++)
        {
            cv::Mat floatImg;
            input["p" + std::to_string(i)] >> floatImg;

            _images[i] = std::make_pair(floatImg, cv::Mat());
        }
    }
    else
    {
        throw std::runtime_error("Only .lst and .yml inputs are supported");
    }
}

ImageList::ImageList(cv::Mat singleImage)
{
    _images.push_back(std::make_pair(singleImage, cv::Mat()));
}

std::pair<cv::Mat, cv::Mat> ImageList::operator [](unsigned int index) const
{
    return _images[index];
}

unsigned int ImageList::count() const
{
    return static_cast<unsigned int>(_images.size());
}
