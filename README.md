Discriminative Hand Tracking
============================

Hand detection/tracking from depth maps using random forest learning. Instead of
using an anatomical model of the hand, we attempt to learn the depth signature of a typical
hand using simple features and random forests. 

Dependencies
------------
* [CMake](http://cmake.org/)
* [freenect](https://github.com/OpenKinect/libfreenect)
* [libusb](http://www.libusb.org/) (Linux users: please install using your package manager)
* [Sherwood](http://research.microsoft.com/en-us/downloads/52d5b9c3-a638-42a1-94a5-d549e2251728/)

Build
-----
* `mkdir build`
* `cd build`
* `cmake ..`
* `make`

Papers
------
* [Ehrlich, Max, *Discriminative Hand Tracking from Depth Images*, Master's Thesis, Stevens Institute of Technology, August 2015.](https://drive.google.com/file/d/0B1F4lLyNBEP_NHptb0hHM2JUcU0/view?usp=sharing)