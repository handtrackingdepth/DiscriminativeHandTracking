#include "CompareTracks.hpp"
#include <fstream>
#include <sstream>
#include <iterator>

void CompareTracks::run()
{
    std::string gtTrackFile = _options["-gt"];
    std::string targetTrackFile = _options["-t"];

    std::string resultFile = _options["-r"];

    track gt = parseTrack(gtTrackFile);
    track t = parseTrack(targetTrackFile);

    auto res = compare(gt, t);

    std::ofstream resFile(resultFile);

    resFile << "mean error: " << res.meanError << std::endl;
    resFile << "median error: " << res.medianError << std::endl;
    resFile << "mean iou: " << res.meanIou << std::endl;
    resFile << "median iou: " << res.medianIou << std::endl;

    bool saveExtended = std::stoi(_options["-se"]);

    if(saveExtended)
    {
        std::string extendedResultsFile = _options["-re"];

        std::ofstream errOut(extendedResultsFile + "_errors");
        std::copy(res.errors.begin(), res.errors.end(), std::ostream_iterator<float>(errOut, "\n"));

        std::ofstream iouOut(extendedResultsFile + "_iou");
        std::copy(res.iou.begin(), res.iou.end(), std::ostream_iterator<float>(iouOut, "\n"));
    }
}

// Compute the area of intersection of the two circles
float CompareTracks::circleInsersection(circle a, circle b)
{
    // Distance between circle centers
    double dist = cv::norm(b.center - a.center);

    // Sort radii
    double rLarge, rSmall;
    std::tie(rSmall,rLarge) = std::minmax(a.radius, b.radius);

    if(dist > rLarge + rSmall) // Case 1: no intersection
    {
        return 0.f;
    }
    else if(dist <= (rLarge - rSmall)) // Case 2: The larger circle fully encloses the smaller circle
    {
        if(a.radius >= b.radius)
            return M_PI * b.radius * b.radius;
        else
            return M_PI * a.radius * a.radius;
    }

    // Case 3: everything else
    double p1 = rSmall*rSmall * std::acos((dist*dist + rSmall*rSmall - rLarge*rLarge)/(2*dist*rSmall));
    double p2 = rLarge*rLarge * std::acos((dist*dist + rLarge*rLarge - rSmall*rSmall)/(2*dist*rLarge));
    double p3 = 0.5 * std::sqrt((-dist+rSmall+rLarge)*(dist+rSmall-rLarge)*(dist-rSmall+rLarge)*(dist+rSmall+rLarge));

    return float(p1 + p2 - p3);
}

track CompareTracks::parseTrack(std::string fileName)
{
    track result;

    std::ifstream file(fileName);

    std::string line;
    while(std::getline(file, line))
    {
        frame currentFrame;

        if(line != "")
        {
            std::istringstream format(line);

            while (format)
            {
                circle c;
                format >> c.center.x;
                format >> c.center.y;
                format >> c.radius;

                if (c.radius > 0)
                    currentFrame.push_back(c);
            }
        }

        result.push_back(currentFrame);
    }

    return result;
}

CompareResult CompareTracks::compare(track gt, track t)
{
    unsigned int size = gt.size();

    CompareResult res;
    res.errors.resize(size);
    res.iou.resize(size);

    for(int i = 0; i < size; i++)
    {
        circle gtCircle = gt[i][0];
        frame targetFrame = t[i];

        float gtCircleArea = M_PI * gtCircle.radius * gtCircle.radius;

        std::vector<float> frameIou(targetFrame.size());
        std::transform(targetFrame.begin(), targetFrame.end(), frameIou.begin(), [&](circle target)
        {
            float targetCircleArea = M_PI * target.radius * target.radius;

            float intersectionArea = circleInsersection(gtCircle, target);

            float unionArea = gtCircleArea + targetCircleArea - intersectionArea;

            return intersectionArea / unionArea;
        });

        auto maxIouPos = std::max_element(frameIou.begin(), frameIou.end());

        if(maxIouPos != frameIou.end())
            res.iou[i] = *maxIouPos;
        else
            res.iou[i] = -1;

        std::vector<float> frameError(targetFrame.size());
        std::transform(targetFrame.begin(), targetFrame.end(), frameError.begin(), [&gtCircle](circle target)
        {
            return cv::norm(gtCircle.center - target.center);
        });

        auto minErrorPos = std::min_element(frameError.begin(), frameError.end());

        if(minErrorPos != frameError.end())
            res.errors[i] = *minErrorPos;
        else
            res.errors[i] = -1;
    }

    std::vector<float> iouFiltered;
    std::vector<float> errorFiltered;

    std::copy_if(res.iou.begin(), res.iou.end(), std::back_inserter(iouFiltered), [](float iou) { return iou > -1; });
    std::copy_if(res.errors.begin(), res.errors.end(), std::back_inserter(errorFiltered), [](float e) { return e > -1; });

    std::sort(iouFiltered.begin(), iouFiltered.end());
    std::sort(errorFiltered.begin(), errorFiltered.end());

    unsigned int iouFilteredSize = iouFiltered.size();
    unsigned int errorFilteredSize = errorFiltered.size();

    float iouSum = std::accumulate(iouFiltered.begin(), iouFiltered.end(), 0.f);
    res.meanIou = iouSum / iouFilteredSize;

    if(iouFilteredSize % 2 == 0)
    {
        res.medianIou = (iouFiltered[iouFilteredSize / 2 - 1] + iouFiltered[iouFilteredSize / 2]) / 2.f;
    }
    else
    {
        res.medianIou = iouFiltered[iouFilteredSize / 2];
    }

    float errorSum = std::accumulate(errorFiltered.begin(), errorFiltered.end(), 0.f);
    res.meanError = errorSum / errorFilteredSize;

    if(errorFilteredSize % 2 == 0)
    {
        res.medianError = (errorFiltered[errorFilteredSize / 2 - 1] + errorFiltered[errorFilteredSize / 2]) / 2.f;
    }
    else
    {
        res.medianError = errorFiltered[errorFilteredSize / 2];
    }

    return res;
}

