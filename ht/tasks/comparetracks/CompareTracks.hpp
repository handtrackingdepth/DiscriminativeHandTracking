#pragma once

#include <vector>
#include <opencv2/opencv.hpp>
#include "../Task.hpp"

struct CompareResult
{
    std::vector<float> iou;
    std::vector<float> errors;

    float meanIou;
    float meanError;

    float medianIou;
    float medianError;
};

typedef std::vector<circle> frame;

typedef std::vector<frame> track;

class CompareTracks : public Task
{
public:
    CompareTracks() = default;

    CompareTracks(std::map<std::string, std::string> options) : Task(options) {}

    void run();

    CompareResult compare(track gt, track t);

private:
    float circleInsersection(circle a, circle b);

    track parseTrack(std::string fileName);

};

