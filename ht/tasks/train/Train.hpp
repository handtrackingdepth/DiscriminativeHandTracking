#pragma once
#include "../Task.hpp"

class Train : public Task
{
public:
    Train() = default;

    Train(std::map<std::string, std::string> options) : Task(options) {}

    void run();
};