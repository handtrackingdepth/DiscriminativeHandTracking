#include "Train.hpp"
#include <classifier.hpp>
#include <imageList.hpp>

void Train::run()
{
    auto modelFileName = _options["-m"];
    auto trainingImages = _options["-i"];

    auto featureType = _options["-f"];

    std::map<std::string, int> trainingParameters;
    trainingParameters["ntrees"] = _options.count("-n") ? std::stoi(_options["-n"]) : 3;
    trainingParameters["depth"] = _options.count("-d") ? std::stoi(_options["-d"]) : 19;
    trainingParameters["candidateThresholds"] = _options.count("-ct") ? std::stoi(_options["-ct"]) : 2;
    trainingParameters["candidateFeatures"] = _options.count("-cf") ? std::stoi(_options["-cf"]) : 100;
    trainingParameters["lowerBound"] = _options.count("-lb") ? std::stoi(_options["-lb"]) : -128;
    trainingParameters["upperBound"] = _options.count("-ub") ? std::stoi(_options["-ub"]) : 128;

    DecisionForest classifier(modelFileName);
    ImageList images(trainingImages);

    FeatureType ft;

    if(featureType == "abs")
        ft = FeatureType::Abs;
    else if(featureType == "unnorm")
        ft = FeatureType::Unnorm;
    else if(featureType == "shotton")
        ft = FeatureType::Shotton;
    else
        throw std::runtime_error("Unrecognized feature type");

    classifier.train(images, ft, trainingParameters);

    classifier.save(modelFileName);
}