#pragma once
#include "../Task.hpp"

class PostProcess : public Task
{
public:
    PostProcess() = default;

    PostProcess(std::map<std::string, std::string> options) : Task(options) {}

    void run();

    std::vector<std::pair<cv::Mat, std::vector<circle>>> process(ImageList &images, bool vebose= false, float minRadius = 20, float threshold = 0.2, int dilationRadius = 5);
};

