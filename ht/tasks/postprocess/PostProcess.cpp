#include <opencv2/opencv.hpp>
#include <imageList.hpp>
#include "PostProcess.hpp"

const cv::Size imageSize(640, 480);

constexpr int imageRegion = 480 / 4;
const int centralProportion = 3;

const float minradius = 20;

void PostProcess::run()
{
    auto imageList = _options["-i"];
    ImageList images(imageList);

    auto outname = _options["-o"];

    bool verbose = std::stoi(_options["-v"]);

    float minRadius = std::stof(_options["-m"]);

    float threshold = std::stof(_options["-t"]);

    int dilationRadius = std::stoi(_options["-d"]);

    bool saveViz = std::stoi(_options["-sv"]);

    auto trackedPoints = process(images, verbose, minRadius, threshold, dilationRadius);

    std::ofstream track(outname);
    track.unsetf(std::ios::floatfield);
    track.precision(10);
    int i = 0;
    for(auto t : trackedPoints)
    {
        if(saveViz)
            cv::imwrite(std::to_string(i++) + ".png", t.first);

        for(auto p : t.second)
        {
            track << p.center.x << " " << p.center.y << " " << p.radius << " ";
        }

        track << std::endl;
    }
}

std::vector<std::pair<cv::Mat, std::vector<circle>>> PostProcess::process(ImageList &images, bool verbose, float minRadius, float threshold, int dilationRadius)
{
    std::vector<std::pair<cv::Mat, std::vector<circle>>> trackedPoints;
    for(int imnum = 0; imnum < images.count(); imnum++)
    {
        cv::Mat originalImg, mask;
        std::tie(originalImg, mask) = images[imnum];


        if(verbose)
            cv::imshow("Original Image", originalImg);

        auto excludedImage = originalImg.clone();

        // Exclude pixels outside of the middle half of the image
        for (int i = 0; i < imageRegion; i++) {
            for (int j = 0; j < imageSize.width; j++) {
                excludedImage.at < float > (i, j) = 0;
            }
        }

        for (int i = imageRegion * centralProportion; i < imageSize.height; i++) {
            for (int j = 0; j < imageSize.width; j++) {
                excludedImage.at < float > (i, j) = 0;
            }
        }

        // Remove pixels with lower than a 90% confidence detection
        cv::threshold(excludedImage, excludedImage, threshold, 255, CV_THRESH_TOZERO);

        if(verbose)
            cv::imshow("Image with Poor Detections Removed", excludedImage);

        // Convert thesholded image to grayscale
        cv::Mat thresholdedImage;
        excludedImage.convertTo(thresholdedImage, CV_8U, 255);

        // Dilate the image
        auto kernel = cv::Mat::ones(7, 7, CV_8U);
        cv::Mat dilated = thresholdedImage.clone();
        //cv::dilate(excludedImage, dilated, kernel);

        if(verbose)
            cv::imshow("Image After Dilation", dilated);

        // Detect and draw contours
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Vec4i> heirarchy;

        cv::findContours(dilated, contours, heirarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
        cv::Mat contourImage = cv::Mat::zeros(imageSize, CV_8UC3);

        for (int i = 0; i < contours.size(); i++)
        {
            cv::Scalar white(255, 255, 255);
            cv::drawContours(contourImage, contours, i, white);
        }

        if(verbose)
            cv::imshow("Contours", contourImage);

        // Find a bounding circle for each contour
        cv::Mat circlesImage = contourImage.clone();

        std::vector<circle *> circles;
        for (auto c : contours) {
            cv::Point2f center;
            float radius;
            cv::minEnclosingCircle(c, center, radius);
            cv::Scalar color(0, 0, 255);
            cv::circle(circlesImage, center, radius, color);

            circles.push_back(new circle{center, radius + dilationRadius});
        }

        if(verbose)
            cv::imshow("Enclosing Circles", circlesImage);

        // Cluster intersecting circles
        std::map<circle *, int> clusters;

        // Start with each circle in its own cluster
        for (int i = 0; i < circles.size(); i++) {
            clusters[circles[i]] = i;
        }

        // Iteratively merge clusters by checking for intersections between circles
        for (circle *c0 : circles) {
            for (circle *c1 : circles) {
                if (c0 != c1) // Don't check for an intersection between a circle and itself
                {
                    // Compute the squared sum the radii
                    float rSumSq = std::pow(c0->radius + c1->radius, 2);

                    // Compute the squared difference in center point coordinates
                    float sqDiffX = std::pow(c0->center.x - c1->center.x, 2);
                    float sqDiffY = std::pow(c0->center.y - c1->center.y, 2);

                    // Compute the squared distance between center points
                    float sqErr = sqDiffX + sqDiffY;

                    // Check for overlap
                    if (sqErr <= rSumSq) {
                        // The circles overlap, merge cluster of c1 into cluster of c0
                        int targetCluster = clusters[c0];
                        int dyingCluster = clusters[c1];

                        for (auto &p : clusters) {
                            if (p.second == dyingCluster) {
                                p.second = targetCluster;
                            }
                        }
                    }
                }
            }
        }

        // Draw each cluster in a new color
        cv::Mat clusteredCirclesImage = contourImage.clone();

        cv::RNG rng(123456789);
        std::map<int, cv::Scalar> colors;
        for (auto p : clusters) {
            if (colors.find(p.second) == colors.end()) {
                cv::Scalar col = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
                colors[p.second] = col;
            }

            cv::circle(clusteredCirclesImage, p.first->center, p.first->radius, colors[p.second]);
        }

        if(verbose)
            cv::imshow("After Clustering", clusteredCirclesImage);

        // Group circles by cluster
        std::vector<std::vector<circle *>> clusterGroups;
        std::vector<int> processedClusters;

        for (auto p : clusters) {
            if (std::find(processedClusters.begin(), processedClusters.end(), p.second) == processedClusters.end()) {
                std::vector<std::pair<circle *, int>> circlesWithClusterNumber;
                std::copy_if(clusters.begin(), clusters.end(), std::back_inserter(circlesWithClusterNumber),
                             [&](auto pair) {
                                 return pair.second == p.second;
                             });

                std::vector<circle *> circlesInCluster;
                std::transform(circlesWithClusterNumber.begin(), circlesWithClusterNumber.end(),
                               std::back_inserter(circlesInCluster), [](auto pair) {
                            return pair.first;
                        });

                clusterGroups.push_back(circlesInCluster);
                processedClusters.push_back(p.second);
            }
        }

        // For each group, compute a bounding circle for the entire group
        std::vector<circle *> boundingCircles;
        std::transform(clusterGroups.begin(), clusterGroups.end(), std::back_inserter(boundingCircles),
                       [](auto &group) {
                           // Compute bounding boxes for each circle
                           std::vector<cv::Rect> bboxes;
                           std::transform(group.begin(), group.end(), std::back_inserter(bboxes), [](circle *c) {
                               float left = c->center.x - c->radius;
                               float top = c->center.y - c->radius;

                               float width, height;
                               width = height = c->radius * 2.f;

                               return cv::Rect(left, top, width, height);
                           });

                           // Find a bounding box that encloses the entire cluster
                           auto minX = std::min_element(bboxes.begin(), bboxes.end(), [](auto first, auto second) {
                               return first.x < second.x;
                           })->x;

                           auto minY = std::min_element(bboxes.begin(), bboxes.end(), [](auto first, auto second) {
                               return first.y < second.y;
                           })->y;

                           auto maxRectX = std::max_element(bboxes.begin(), bboxes.end(), [](auto first, auto second) {
                               return first.x + first.width < second.x + second.width;
                           });
                           auto maxX = maxRectX->x + maxRectX->width;

                           auto maxRectY = std::max_element(bboxes.begin(), bboxes.end(), [](auto first, auto second) {
                               return first.y + first.height < second.y + first.height;
                           });
                           auto maxY = maxRectY->y + maxRectY->height;

                           cv::Rect clusterBox(minX, minY, maxX - minX, maxY - minY);

                           // Inscribe a circle inside the cluster bounding box
                           circle *clusterCircle = new circle();

                           clusterCircle->center = cv::Point2f(
                                   clusterBox.x + clusterBox.width / 2.f,
                                   clusterBox.y + clusterBox.height / 2.f
                           );

                           clusterCircle->radius = std::min(clusterBox.width / 2.f, clusterBox.height / 2.f);

                           return clusterCircle;
                       });

        // Draw the cluster circles
        cv::Mat clustersImage = contourImage.clone();

        for (auto c : boundingCircles) {
            cv::Scalar col = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
            cv::circle(clustersImage, c->center, c->radius, col);
        }

        if(verbose)
            cv::imshow("After Clustering (2)", clustersImage);

        // Exclude circles smaller than a fixed radius
        auto nend = std::remove_if(boundingCircles.begin(), boundingCircles.end(),
                                   [&](auto c) { return c->radius < minradius; });
        boundingCircles.erase(nend, boundingCircles.end());

        // Report the remaining circle centers as the tracked hand point
        cv::Mat finalCircles = contourImage.clone();

        std::vector<circle> currentFrameHands;
        for (auto c : boundingCircles)
        {
            cv::Scalar col = cv::Scalar(rng.uniform(100, 255), rng.uniform(100, 170), rng.uniform(100, 255));
            cv::circle(finalCircles, c->center, c->radius, col);

            cv::circle(finalCircles, c->center, 1, col);
            currentFrameHands.push_back(*c);
        }

        trackedPoints.push_back(std::make_pair(finalCircles, currentFrameHands));

        if(verbose)
        {
            cv::imshow("Final Result, Region and Tracked Hand Point", finalCircles);
            cv::waitKey(0);
        }

        for (auto c : circles)
            delete c;

        for (auto c : boundingCircles)
            delete c;
    }

    return trackedPoints;
}
