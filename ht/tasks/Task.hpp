#pragma once

#include <string>
#include <map>
#include <memory>
#include <opencv2/opencv.hpp>

struct circle
{
    cv::Point2f center;
    float radius;
};

class Task
{
public:
    static std::unique_ptr<Task> build(std::map<std::string, std::string> args);

    virtual void run() = 0;

protected:
    Task(std::map<std::string, std::string> options) : _options(options) { }

    Task() = default;

    std::map<std::string, std::string> _options;

};
