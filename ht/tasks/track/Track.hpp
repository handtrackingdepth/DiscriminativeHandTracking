#pragma once
#include "../Task.hpp"

class Track : public Task
{
public:
    Track(std::map<std::string, std::string> options) : Task(options) {}

    void run();
};