#include "Track.hpp"
#include <opencvKinectDevice.hpp>
#include "../test/Test.hpp"
#include "../postprocess/PostProcess.hpp"



void Track::run()
{
    Test t;
    PostProcess pp;

    std::cout << "Loading Model...";

    auto modelFileName = _options["-m"];

    DecisionForest classifier(modelFileName);

    std::cout << "Done" << std::endl;

    std::cout << "Initializing Kinect...";

    Freenect::Freenect freenect;
    std::unique_ptr<opencvKinectDevice> device(&freenect.createDevice<opencvKinectDevice>(0));

    device->startVideo();
    device->startDepth();

    std::cout << "Done" << std::endl;

    std::cout << "Starting Hand tracking, press 'q' to quit" << std::endl;

    char press = '\0';

    while(press != 'q')
    {
        auto before = std::chrono::system_clock::now();

        cv::Mat probe;
        device->grayDepth(probe);

        cv::Mat reference;
        device->color(reference);

        cv::imshow("Color", reference);
        cv::imshow("Depth", probe);

        ImageList singleForTesting(probe);
        auto testResult = t.doTesting(singleForTesting, classifier);

        cv::Mat graydepthTestResult;
        testResult[0].convertTo(graydepthTestResult, CV_8U, 255);

        ImageList singleForProcessing(graydepthTestResult);
        auto spatialPostProcessingResult = pp.process(singleForProcessing);

        // TODO temporal post processing

        auto finalRes = spatialPostProcessingResult[0];

        for(auto pt : finalRes.second)
        {
            std::cout << "[" << pt.center.x << " " << pt.center.y << "] ";
        }
        std::cout << std::endl;

        cv::imshow("Result", finalRes.first);

        auto after = std::chrono::system_clock::now();

        auto duration = after - before;
        auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

        float fps = 1000.f / durationMS;

        std::cout << fps << " fps" << std::endl;

        press = cv::waitKey(1);
    }

    std::cout << "Shutting Down Kinect...";

    device->stopVideo();
    device->stopDepth();

    std::cout << "Done" << std::endl;
}