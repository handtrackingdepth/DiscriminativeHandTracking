#include "Test.hpp"
#include <classifier.hpp>
#include <imageList.hpp>

void Test::run()
{
    auto modelFileName = _options["-m"];
    auto imageList = _options["-i"];
    bool saveViz = std::stoi(_options["-sv"]);

    DecisionForest classifier(modelFileName);
    ImageList images(imageList);

    std::vector<cv::Mat> labels = doTesting(images, classifier);

    cv::FileStorage pout("probability.yml", cv::FileStorage::WRITE);
    pout << "size" << int(labels.size());
    for(int i = 0; i < labels.size(); i++)
    {
        pout << "p" + std::to_string(i) << labels[i];

        if(saveViz)
        {
            cv::Mat temp;
            labels[i].convertTo(temp, CV_8U, 255);

            cv::Mat colout;
            cv::applyColorMap(temp, colout, cv::COLORMAP_SUMMER);

            cv::imwrite(std::to_string(i) + "_viz.png", colout);
        }
    }
}

std::vector<cv::Mat> Test::doTesting(ImageList &input, DecisionForest &classifier)
{
    return classifier.test(input);
}
