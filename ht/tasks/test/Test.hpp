#pragma once
#include <vector>
#include <imageList.hpp>
#include <classifier.hpp>
#include "../Task.hpp"

class Test : public Task
{
public:
    Test() = default;

    Test(std::map<std::string, std::string> options) : Task(options) {}

    void run();

    std::vector<cv::Mat> doTesting(ImageList &input, DecisionForest &classifier);
};

