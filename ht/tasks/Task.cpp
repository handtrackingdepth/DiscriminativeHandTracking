#include "Task.hpp"
#include "train/Train.hpp"
#include "test/Test.hpp"
#include "postprocess/PostProcess.hpp"
#include "track/Track.hpp"
#include "comparetracks/CompareTracks.hpp"

std::unique_ptr<Task> Task::build(std::map<std::string, std::string> options)
{
    if(options["action"] == "train")

        return std::unique_ptr<Task>(new Train(options));
    else if(options["action"] == "test")
        return std::unique_ptr<Task>(new Test(options));
    else if(options["action"] == "postprocess")
        return std::unique_ptr<Task>(new PostProcess(options));
    else if(options["action"] == "track")
        return std::unique_ptr<Task>(new Track(options));
    else if(options["action"] == "comparetracks")
        return std::unique_ptr<Task>(new CompareTracks(options));

    return nullptr;
}