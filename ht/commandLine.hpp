#pragma once

#include <string>
#include <map>

std::map<std::string, std::string> parseCommandLine(int argc, char **argv)
{
    std::map<std::string, std::string> options;
    options["action"] = argv[1];

    for(int i = 2; i < argc; i+=2)
    {
        options[argv[i]] = argv[i+1];
    }

    return options;
}
