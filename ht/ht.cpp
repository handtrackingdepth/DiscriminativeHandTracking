#include "commandLine.hpp"
#include "tasks/Task.hpp"
#include <iostream>

int main(int argc, char **argv)
{
    try
    {
        auto args = parseCommandLine(argc, argv);

        std::unique_ptr<Task> task = Task::build(args);
        task->run();
    }
    catch (std::exception &e)
    {
        std::cerr << "ERROR " << e.what() << std::endl;
    }

    return 0;
}
