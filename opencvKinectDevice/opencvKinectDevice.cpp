#include "opencvKinectDevice.hpp"

opencvKinectDevice::opencvKinectDevice(freenect_context *_ctx, int _index)
    : Freenect::FreenectDevice(_ctx, _index), _depth(cv::Size(640,480),CV_16U, cv::Scalar(0)), _color(cv::Size(640,480),CV_8UC3, cv::Scalar(0))
{

}

void opencvKinectDevice::VideoCallback(void *rgb, uint32_t timestamp)
{
    std::lock_guard<std::mutex> locker(_colorLock);

    cv::Mat rgbMat(cv::Size(640, 480), CV_8UC3, rgb);
    cv::cvtColor(rgbMat, _color, CV_RGB2BGR);
}

void opencvKinectDevice::DepthCallback(void *depth, uint32_t timestamp)
{
    std::lock_guard<std::mutex> locker(_depthLock);

    cv::Mat depthImg16(cv::Size(640, 480), CV_16U, depth);
    depthImg16.copyTo(_depth);
}

void opencvKinectDevice::color(cv::Mat &output)
{
    std::lock_guard<std::mutex> locker(_colorLock);
    _color.copyTo(output);
}

void opencvKinectDevice::rawDepth(cv::Mat &output)
{
    std::lock_guard<std::mutex> locker(_depthLock);
    _depth.copyTo(output);
}

void opencvKinectDevice::grayDepth(cv::Mat &output)
{
    std::lock_guard<std::mutex> locker(_depthLock);
    _depth.convertTo(output, CV_8U, 255.0/2048.0); // Scale from 2048 max
}

void opencvKinectDevice::worldDepth(cv::Mat &output)
{
    std::lock_guard<std::mutex> locker(_depthLock);

    output.create(_depth.rows, _depth.cols, CV_64FC3);

    const double k1 = 1.1863;
    const double k2 = 2842.5;
    const double k3 = 0.1236;

    const double minDist = -10;
    const double scale = 0.0021;

    for(int j = 0; j < _depth.rows; j++)
    {
        for(int i = 0; i < _depth.cols; i++)
        {
            unsigned short rawDepth = _depth.at<unsigned short>(j, i);

            double z = k3 * std::tan(((double)rawDepth / k2) + k1);
            double x = ((double)i - ((double) _depth.cols / 2.0)) * (z + minDist) * scale;
            double y = ((double)j - ((double) _depth.rows / 2.0)) * (z + minDist) * scale;

            cv::Vec3d el;
            el[0] = x;
            el[1] = y;
            el[2] = z;

            output.at<cv::Vec3d>(j, i) = el;
        }
    }
}

void opencvKinectDevice::tilt(double angle)
{
    setTiltDegrees(angle);
}

double opencvKinectDevice::tilt(void)
{
    updateState();

    double angle = getState().getTiltDegs();
    return angle;
}

void opencvKinectDevice::tiltUp(double dt)
{
    updateState();

    double angle = getState().getTiltDegs();
    setTiltDegrees(angle + dt);
}

void opencvKinectDevice::tiltDown(double dt)
{
    updateState();

    double angle = getState().getTiltDegs();
    setTiltDegrees(angle - dt);
}
