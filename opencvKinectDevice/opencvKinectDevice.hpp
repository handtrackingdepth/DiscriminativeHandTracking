#include <libfreenect/libfreenect.hpp>
#include <opencv2/opencv.hpp>
#include <mutex>
#include <vector>

class opencvKinectDevice : public Freenect::FreenectDevice
{
  public:
    opencvKinectDevice(freenect_context *_ctx, int _index);

    void VideoCallback(void* _rgb, uint32_t timestamp);

    void DepthCallback(void* _depth, uint32_t timestamp);

    void tiltUp(double dt = 5);
    void tiltDown(double dt = 5);

    void tilt(double angle);
    double tilt(void);

    void color(cv::Mat &output);

    void rawDepth(cv::Mat &output);
    void grayDepth(cv::Mat &output);
    void worldDepth(cv::Mat &output);

  private:
    cv::Mat _depth;
    cv::Mat _color;

    std::mutex _colorLock;
    std::mutex _depthLock;
};

