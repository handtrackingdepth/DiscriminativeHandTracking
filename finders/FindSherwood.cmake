# - Find Sherwood decision forest library (headers only library)

if (SHERWOOD_INCLUDE_DIRS)
  set(SHERWOOD_FOUND TRUE)
else (SHERWOOD_INCLUDE_DIRS)
  find_path(SHERWOOD_INCLUDE_DIR
    NAMES
	Sherwood.h
    PATHS
      /usr/include/*
      /usr/local/include/*
  )

  set(SHERWOOD_INCLUDE_DIRS
    ${SHERWOOD_INCLUDE_DIR}
  )

  if (SHERWOOD_INCLUDE_DIRS)
     set(SHERWOOD_FOUND TRUE)
  endif (SHERWOOD_INCLUDE_DIRS)

  if (SHERWOOD_FOUND)
    if (NOT SHERWOOD_FIND_QUIETLY)
      message(STATUS "Found SHERWOOD:")
	  message(STATUS " - Includes: ${SHERWOOD_INCLUDE_DIRS}")
    endif (NOT SHERWOOD_FIND_QUIETLY)
  else (SHERWOOD_FOUND)
    if (SHERWOOD_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find Sherwood")
    endif (SHERWOOD_FIND_REQUIRED)
  endif (SHERWOOD_FOUND)

endif (SHERWOOD_INCLUDE_DIRS)
